# 29/11/2021
- Added the Transform_template_to_sdmxcsv.py script to transform the reporting template to SDMX-CSV, explained in the user guide

# 10/11/2021
## New
- Existing codelists can be referenced with 3-part identifier in Concept Scheme. Avoids requiring a codelist worksheet
- Generate a reporting template with guidelines
- Substantial improvements to user guide 

## Fixed
- Dimension Position attribute behaviour changed:
    - Position values are not now auto-generated 
    - If there are Position values, 
        - all non-time dimensions must have Position
        - The order of the dimensions in the SDMX-ML follows the Position values
- Changed all isFinal cells to Text, hopefully avoids true/false being translated in SDMX-ML
- Changed all integer variables to long to avoid problems with long lists
- Disable Delete rows in DF, DSD and Concept Scheme worksheets to avoid corrupting named ranges
- Fixed duplicate ID conditional formatting for codelists
- Removed duplicate Name conditional formatting for codelists - it was confusing

# 20/8/2021
## New
- Add attribute group definitions in Concept Scheme worksheet (for DSDs)

## Fixed 
- Prefill errors if structure contains keyset content constraints
- Change IDs of microdata-related annotation types
- Other bug fixes and performance improvements

# 16/6/2021
## Fixed 
- Fix slow generation time (slow Find problem in constraints)

# 15/6/2021
## New
- Allow skipping the generation of codelists to avoid overwriting non-final codelists when collaborating
- Codelist code orders: Auto generate of order required when only a subset of codes have an order
- Add "MAXTEXTATTRIBUTELENGTH" column to the DSDs sheet

## Fixed
- Improved speed of prefilling codelists
- Change UNIT_MULT default attachment to series
- Matrix Generator highlights codes with red on CL sheets that are actually not duplicates
- Concept number in a concept scheme is used as dimension position ID if Position is empty on the Concept Scheme sheet

# 22/4/2021
## Fixed
- DSD to take more than the first 99 concepts by default
- Add "MAXTEXTATTRIBUTELENGTH" column to the DSDs sheet
- Change IDs of microdata-related annotation types
- Issues with generation of annotations
- Invalid SDMX code is generated when DSD structure has no attributes
- Dimensions Position attribute ignored when prefilling
- Increase allowed number of used concepts in a DSD to 999

# 10/3/2021
## Fixed
- Prefilling artefacts w/o isFinal attribute causes error
- Support of large codelists in Matrix Generator. Now supports up to 500000 codes.

# 27/11/2020
## New
- Add MAXTEXTATTRIBUTELENGTH DSD annotation
- Make Constraint IDs editable. When prefilling show them on Dataflow worksheet
- Add named ranges to make it easier for programs that wrap the application
- Add UNIT_MEASURE_CONCEPTS Dataflow annotation
- Add DSD annotations

## Fixed
- Dimension with role D(Frequency) is skipped when generating attachment relationship for time-series attribute [github#68](https://github.com/OECDSTD/sdmx-matrix-generator/issues/68)
- Prefill from Rest API goes in error with Excel language settings "German"
- Locale language setting in Excel different from English [github#53](https://github.com/OECDSTD/sdmx-matrix-generator/issues/53)
- Prefill function errors when regional settings include separators that are incompatible with adding hyperlinks
- Prefilling Annotations errors because it assumes several properties are mandatory. They're not in SDMX

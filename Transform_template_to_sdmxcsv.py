import sys
import os
import argparse
import warnings
import csv
import datetime
import pandas as pd
import numpy as np
# By using the following package, if you try to update the constant value, ConstantError will be raised
from pconst import const
from pathlib import Path    

# Declare constants
const.REPORTING_TEMPLATE_SHEET_NAME = "Reporting template"
const.DATA_FLOW_HEADER = "Dataflow Id"
const.CONCEPT_HEADER  = "Concept Id"
const.DATAFLOW = "DATAFLOW"
const.UNCODED = "UNCODED"
const.ERROR_MESSAGE = "An error occurred: "


# Function to check if the local file exists
def file_exists(template_path):
   Returned_Message = ""

   try:
      if os.path.exists(template_path):
         return template_path
      else:
         return const.ERROR_MESSAGE + " File doesn't exist"

   except Exception as err:
      Returned_Message = const.ERROR_MESSAGE + str(err)
      print(Returned_Message)

   finally:
      if Returned_Message == "":
         return template_path
      else:
         return Returned_Message

# Populate the data list using the concepts list and the corresponding codelist worksheet
def PopulateDatalist(All_worksheets, Concepts_List):

   ReportingTemplateWorksheet = All_worksheets[const.REPORTING_TEMPLATE_SHEET_NAME]
   # Change the header of the "ReportingTemplate" worksheet to the "concepts" row
   new_header = ReportingTemplateWorksheet.iloc[0] # Grab the first row for the header
   ReportingTemplateWorksheet = ReportingTemplateWorksheet[1:] # Take the data less the header row
   ReportingTemplateWorksheet.columns = new_header # Set the header row as the df header

   Data_List = []
   for concept in Concepts_List:
      CodelistValue = ReportingTemplateWorksheet[concept].to_numpy()[0]

      Values_List = []
      if CodelistValue.upper() != const.UNCODED:
         warnings.simplefilter("ignore")
         CodelistWorksheet = All_worksheets[CodelistValue]
         warnings.simplefilter("default")

         # Change the header of each "Codelist" worksheet to the header of the data row
         new_header = CodelistWorksheet.iloc[12] # Grab the first row for the header
         CodelistWorksheet = CodelistWorksheet[13:] # Take the data less the header row
         CodelistWorksheet.columns = new_header # Set the header row as the df header

         # Loop through the current column rows
         index = 2
         while index < len(ReportingTemplateWorksheet[concept].to_numpy()):
               Value = ReportingTemplateWorksheet[concept].to_numpy()[index]
               if pd.isnull(Value):
                  Values_List.append(None)
               else:
                  ValueIndex = CodelistWorksheet["Name:en"].to_numpy().tolist().index(Value)
                  ValueCode = CodelistWorksheet["Code*"].to_numpy()[ValueIndex]
                  Values_List.append(ValueCode)
               index += 1
         Data_List.append(Values_List)
      else:
         index = 2
         while index < len(ReportingTemplateWorksheet[concept].to_numpy()):
               Value = ReportingTemplateWorksheet[concept].to_numpy()[index]
               if pd.isnull(Value):
                  Values_List.append(None)
               else:
                  if isinstance(Value, str):
                     if len(Value) > 0:
                           Value = Value.replace("\n", " ").strip() 
                     Values_List.append(Value)
                  elif isinstance(Value, datetime.date):
                     Values_List.append(Value.date().strftime("%Y-%m-%d"))
                  else:
                     Values_List.append(Value)
               index += 1
         Data_List.append(Values_List)

   return Data_List


# Create a CSV file and populate it using Headers_List and Data_List
def CreateCSVFile(sdmxcsv_path, Headers_List, Data_List):
   try:
      Returned_Message = ""

      with open(sdmxcsv_path, 'w', encoding='UTF8', newline='') as f:
         writer = csv.writer(f)
         # write the header
         writer.writerow(Headers_List)
         # write the data
         writer.writerows(Data_List)
   except Exception as err:
      Returned_Message = const.ERROR_MESSAGE + str(err) 

   if Returned_Message == "":
      return sdmxcsv_path
   else:
      return Returned_Message


# --template_path "C:\Temp\MyProjects\CSV_File_Location\DTI_ReportingTemplate.xlsx" --sdmxcsv_path "C:\Temp\MyProjects\CSV_File_Location\DTI_ReportingTemplate.csv"
# --template_path "C:\Temp\MyProjects\CSV_File_Location\DTI_COLL_ReportingTemplate_test.xlsx" --sdmxcsv_path "C:\Temp\MyProjects\CSV_File_Location\DTI_COLL_ReportingTemplate_test.csv"
# --template_path \\main.oecd.org\em_data\DTPD_TEST --sdmxcsv_path \\main.oecd.org\em_data\DTPD_TEST
# 
def main():
   try:
      Returned_Message = ""

      parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
      parser.add_argument('--template_path', required=True, help="The path of a single '.xlsx' template file or the path of the directory of the '.xlsx' files")
      parser.add_argument('--sdmxcsv_path', required=True, help="The path of a single SDMX-CSV file or the path of the directory of the SDMX-CSV files")

      args = parser.parse_args(sys.argv[1:])
      template_path = args.template_path
      sdmxcsv_path = args.sdmxcsv_path

      Template_Path_List = []
      Sdmxcsv_Path_List = []
      if os.path.isdir(template_path) and os.path.isdir(sdmxcsv_path):  
         for template_filename in os.listdir(template_path):
            if os.path.isfile(template_path + '\\' + template_filename) and os.path.splitext(template_filename)[-1].lower() == ".xlsx":
               Template_Path_List.append(template_path + '\\' + template_filename)
               sdmxcsv_fullpath = sdmxcsv_path + '\\' + Path(template_filename).stem + '.csv'
               Sdmxcsv_Path_List.append(sdmxcsv_fullpath)
      elif os.path.isfile(template_path):  
         Template_Path_List.append(template_path)
         if os.path.isdir(sdmxcsv_path):
            sdmxcsv_fullpath = sdmxcsv_path + '\\' + Path(template_path).stem + '.csv'
         else:
            sdmxcsv_fullpath = sdmxcsv_path
         Sdmxcsv_Path_List.append(sdmxcsv_fullpath)
      else:
         Returned_Message = const.ERROR_MESSAGE + "Check arguments '" + template_path + "' and '" + sdmxcsv_path + "'"

      # 
      if len(Template_Path_List) > 0:
         Returned_Message = ""
      for index in range(len(Template_Path_List)):
         template_path = Template_Path_List[index]
         sdmxcsv_path = Sdmxcsv_Path_List[index]

         warnings.simplefilter("ignore")
         All_worksheets = pd.read_excel(template_path, sheet_name=None)
         ReportingTemplateWorksheet = All_worksheets[const.REPORTING_TEMPLATE_SHEET_NAME]
         warnings.simplefilter("default")

         # Get DataFlow Id
         DataFlowId = ReportingTemplateWorksheet.columns.to_numpy()[1]

         # Get Concept list
         Concepts_List = []
         index = 1
         while index < len(ReportingTemplateWorksheet.to_numpy()[0].tolist()):
            Concepts_List.append(ReportingTemplateWorksheet.to_numpy()[0].tolist()[index])
            index += 1

         # Populate Header list
         Headers_List = [const.DATAFLOW]
         for item in Concepts_List:
            Headers_List.append(item)

         # Populate Data list
         Data_List = PopulateDatalist(All_worksheets, Concepts_List)

         # Data_List is pupulated with data by column, it needs to be reversed
         # Transpose "Numpy_Array"
         Numpy_Array = np.array(Data_List)
         Transpose = Numpy_Array.T
         Transpose_list = Transpose.tolist()

         # Remove empty rows
         Data_List.clear()
         for row in Transpose_list:
            NotEmpty = False
            for Value in row:
               if Value != None:
                  NotEmpty = True
                  break
            if NotEmpty != False:
               row.insert(0, DataFlowId) 
               Data_List.append(row)

         # Create a CSV file and populate it with data
         Result = CreateCSVFile(sdmxcsv_path, Headers_List, Data_List)
         Returned_Message = Returned_Message + "\n" + Result
   except Exception as err:
      Returned_Message = const.ERROR_MESSAGE + str(err)
   finally:
      return Returned_Message
         

if __name__ == '__main__':
   sys.stdout.write(main())
